/*
Copyright (C) 2022 CERN for the benefit of the FASER collaboration
*/

#ifndef MYEXTRAPOLATIONEXAMPLE_MYEXTRAPOLATIONEXAMPLE_H
#define MYEXTRAPOLATIONEXAMPLE_MYEXTRAPOLATIONEXAMPLE_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "FaserActsGeometryInterfaces/IFaserActsExtrapolationTool.h"
#include "GeneratorObjects/McEventCollection.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"

class FaserSCT_ID;
namespace  TrackerDD {
class SCT_DetectorManager;

}
namespace Tracker {

class MyExtrapolationExample : public AthReentrantAlgorithm {
 public:
  MyExtrapolationExample(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;

private:
  double m_MeV2GeV = 1e-3;
  const FaserSCT_ID* m_idHelper {nullptr};
  const TrackerDD::SCT_DetectorManager* m_detMgr {nullptr};
  SG::ReadHandleKey<McEventCollection> m_mcEventCollectionKey {
      this, "McEventCollection", "TruthEvent"};
  SG::ReadHandleKey <FaserSiHitCollection> m_faserSiHitKey {
    this, "FaserSiHitCollection", "SCT_Hits"};
  ToolHandle<IFaserActsExtrapolationTool> m_extrapolationTool {
    this, "ExtrapolationTool", "FaserActsExtrapolationTool"};
};

} // namespace Tracker

#endif // MYEXTRAPOLATIONEXAMPLE_MYEXTRAPOLATIONEXAMPLE_H

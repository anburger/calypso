# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
#from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg

Tracker__TrackerSPFit, THistSvc=CompFactory.getComps("Tracker::TrackerSPFit", "THistSvc")


def TrackerSPFitBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TrackerSPFit"""
    acc = FaserSCT_GeometryCfg(flags)
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    kwargs.setdefault("SCT_ClustersName", "SCT_ClusterContainer")
    kwargs.setdefault("MaxChi2", 100)
    kwargs.setdefault("UseBiasedResidual", True)
    acc.addEventAlgo(Tracker__TrackerSPFit(**kwargs))
   # attach ToolHandles
    return acc

def TrackerSPFit_OutputCfg(flags):
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
    acc.merge(OutputStreamCfg(flags, "ESD"))
    ostream = acc.getEventAlgo("OutputStreamESD")
    ostream.TakeItemsFromInput = True
    return acc

def TrackerSPFitCfg(flags, **kwargs):
    acc=TrackerSPFitBasicCfg(flags, **kwargs)
    histSvc= THistSvc()
    histSvc.Output += [ "TrackerSPFit DATAFILE='trackerspfit.root' OPT='RECREATE'" ]
    acc.addService(histSvc)
    acc.merge(TrackerSPFit_OutputCfg(flags))
    return acc

if __name__ == "__main__":
  log.setLevel(DEBUG)
  Configurable.configurableRun3Behavior = True

  # Configure
  ConfigFlags.Input.Files = ['/eos/project-f/faser-commissioning/TI12Data/Run-004272/Faser-Physics-004272-00010.raw']
  ConfigFlags.Output.ESDFileName = "mySeeds.ESD.pool.root"
  ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
  ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now
  ConfigFlags.Input.ProjectName = "data21"                     # Needed to bypass autoconfig
  ConfigFlags.Input.isMC = False
  # Needed to bypass autoconfig
  ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # FASER cosmic ray geometry (station 2 only)
  ConfigFlags.Common.isOnline = False
  ConfigFlags.GeoModel.Align.Dynamic = False
  ConfigFlags.Beam.NumberOfCollisions = 0.

  ConfigFlags.lock()

  # Core components
  acc = MainServicesCfg(ConfigFlags)
  acc.merge(PoolWriteCfg(ConfigFlags))

  from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
  acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))
  acc.merge(WaveformReconstructionCfg(ConfigFlags))
  acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags, DataObjectName="SCT_EDGEMODE_RDOs"))
#  acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
  acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
  acc.merge(TrackerSPFitCfg(ConfigFlags))
#  from AthenaConfiguration.ComponentFactory import CompFactory
#  decoderTool = CompFactory.RawWaveformDecoderTool(name = "RawWaveformDecoderTool", 
##      CaloChannels = [0, 1, 2,3, 4, 5], 
##    PreshowerChannels = [6,7], 
##    TriggerChannels = [8, 9],
#      VetoChannels=[])
#  acc.addPublicTool(decoderTool)

  # Timing
  #acc.merge(MergeRecoTimingObjCfg(ConfigFlags))

  # Dump config
  logging.getLogger('forcomps').setLevel(INFO)
  acc.foreach_component("*").OutputLevel = INFO
  acc.foreach_component("*ClassID*").OutputLevel = INFO
  # acc.getCondAlgo("FaserSCT_AlignCondAlg").OutputLevel = VERBOSE
  # acc.getCondAlgo("FaserSCT_DetectorElementCondAlg").OutputLevel = VERBOSE
  acc.getService("StoreGateSvc").Dump = True
  acc.getService("ConditionStore").Dump = True
  acc.printConfig(withDetails=True)
  ConfigFlags.dump()

  # Execute and finish
  sc = acc.run(maxEvents=-1)

  # Success should be 0
  sys.exit(not sc.isSuccess())

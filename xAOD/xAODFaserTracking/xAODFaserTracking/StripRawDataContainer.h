#ifndef XAODFASERTRACKING_STRIPRAWDATACONTAINER_H
#define XAODFASERTRACKING_STRIPRAWDATACONTAINER_H

// Core include(s):
#include "AthContainers/DataVector.h"

// Local include(s):
#include "xAODFaserTracking/StripRawData.h"

namespace xAOD {
   /// The container is a simple typedef for now
   typedef DataVector<xAOD::StripRawData> StripRawDataContainer;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::StripRawDataContainer, 1312857244, 1)

#endif // XAODFASERTRACKING_STRIPRAWDATACONTAINER_H
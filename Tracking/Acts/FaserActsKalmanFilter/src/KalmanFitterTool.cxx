#include "KalmanFitterTool.h"

#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"
#include "TrackerIdentifier/FaserSCT_ID.h"

#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"


KalmanFitterTool::KalmanFitterTool(const std::string& type, const std::string& name, const IInterface* parent) :
    AthAlgTool(type, name, parent) {}


StatusCode KalmanFitterTool::initialize() {
  ATH_CHECK(m_fieldCondObjInputKey.initialize());
  ATH_CHECK(m_trackingGeometryTool.retrieve());
  ATH_CHECK(m_createTrkTrackTool.retrieve());
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
  if (m_statesWriter && !m_noDiagnostics) ATH_CHECK(m_trajectoryStatesWriterTool.retrieve());
  if (m_summaryWriter && !m_noDiagnostics) ATH_CHECK(m_trajectorySummaryWriterTool.retrieve());
  m_fit = makeTrackFitterFunction(m_trackingGeometryTool->trackingGeometry());
  if (m_actsLogging == "VERBOSE" && !m_noDiagnostics) {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::VERBOSE);
  } else if (m_actsLogging == "DEBUG" && !m_noDiagnostics) {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::DEBUG);
  } else {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::INFO);
  }
  return StatusCode::SUCCESS;
}


StatusCode KalmanFitterTool::finalize() {
  return StatusCode::SUCCESS;
}

//get the unbiased residual for the cluster at Z =  clusz
//output is a vector of the outliers
//
std::vector<TSOS4Residual>
KalmanFitterTool::getUnbiasedResidual(const EventContext &ctx, const Acts::GeometryContext &gctx,
                      Trk::Track* inputTrack, double clusz, const Acts::BoundVector& inputVector,
                      bool /*isMC*/, double origin) const {
  std::vector<TSOS4Residual> resi;
  resi.clear();
  std::vector<FaserActsRecMultiTrajectory> myTrajectories;
  ATH_MSG_DEBUG("start kalmanfilter "<<inputTrack->measurementsOnTrack()->size()<<" "<<origin<<" "<<clusz);

  if (!inputTrack->measurementsOnTrack() || inputTrack->measurementsOnTrack()->size() < m_minMeasurements) {
    ATH_MSG_DEBUG("Input track has no or too little measurements and cannot be fitted");
    return resi;
  }

  if (!inputTrack->trackParameters() || inputTrack->trackParameters()->empty()) {
    ATH_MSG_DEBUG("Input track has no track parameters and cannot be fitted");
    return resi;
  }


  auto pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  Acts::MagneticFieldContext mfContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext = Acts::CalibrationContext();

  auto [sourceLinks, measurements] = getMeasurementsFromTrack(inputTrack);
  auto trackParameters = getParametersFromTrack(inputTrack->trackParameters()->front(), inputVector, origin);
  ATH_MSG_DEBUG("trackParameters: " << trackParameters.parameters().transpose());
  ATH_MSG_DEBUG("position: " << trackParameters.position(gctx).transpose());
  ATH_MSG_DEBUG("momentum: " << trackParameters.momentum().transpose());
  ATH_MSG_DEBUG("charge: " << trackParameters.charge());
  ATH_MSG_DEBUG("size of measurements : " << sourceLinks.size());

  FaserActsOutlierFinder faserActsOutlierFinder{0};
  faserActsOutlierFinder.cluster_z=clusz;
  faserActsOutlierFinder.StateChiSquaredPerNumberDoFCut=10000.;
  Acts::KalmanFitterOptions<MeasurementCalibrator, FaserActsOutlierFinder, Acts::VoidReverseFilteringLogic>
      kfOptions(gctx, mfContext, calibContext, MeasurementCalibrator(measurements),
                faserActsOutlierFinder, Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                //FaserActsOutlierFinder(), Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                //Acts::VoidOutlierFinder(), Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                Acts::PropagatorPlainOptions(), &(*pSurface));
  kfOptions.multipleScattering = false;
  kfOptions.energyLoss = false;

  ATH_MSG_DEBUG("Invoke fitter");
  auto result = (*m_fit)(sourceLinks, trackParameters, kfOptions);

  if (result.ok()) {
    const auto& fitOutput = result.value();
    if (fitOutput.fittedParameters) {
      const auto& params = fitOutput.fittedParameters.value();
      ATH_MSG_DEBUG("ReFitted parameters");
      ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
      ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
      ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
      ATH_MSG_DEBUG("  charge:   " << params.charge());
      using IndexedParams = std::unordered_map<size_t, Acts::BoundTrackParameters>;
      IndexedParams indexedParams;
      indexedParams.emplace(fitOutput.lastMeasurementIndex, std::move(params));
      std::vector<size_t> trackTips;
      trackTips.reserve(1);
      trackTips.emplace_back(fitOutput.lastMeasurementIndex);
      myTrajectories.emplace_back(std::move(fitOutput.fittedStates),
                                  std::move(trackTips),
                                  std::move(indexedParams));
      for (const FaserActsRecMultiTrajectory &traj : myTrajectories) {
	const auto& mj = traj.multiTrajectory();
	const auto& trackTips = traj.tips();
	if(trackTips.empty())continue;
	auto& trackTip = trackTips.front();
	mj.visitBackwards(trackTip, [&](const auto &state) {
	    auto typeFlags = state.typeFlags();
	    if (not typeFlags.test(Acts::TrackStateFlag::OutlierFlag))
	    {
	    return true;
	    }

	    if (state.hasUncalibrated()&&state.hasSmoothed()) {
	Acts::BoundTrackParameters parameter(
	    state.referenceSurface().getSharedPtr(),
	    state.smoothed(),
	    state.smoothedCovariance());
	    // auto covariance = state.smoothedCovariance();
	    auto H = state.effectiveProjector();
	    auto residual = state.effectiveCalibrated() - H * state.smoothed();
	    const Tracker::FaserSCT_Cluster* cluster = state.uncalibrated().hit();
	    // const auto& surface = state.referenceSurface();
	    Acts::BoundVector meas = state.projector().transpose() * state.calibrated();
	    Acts::Vector2 local(meas[Acts::eBoundLoc0], meas[Acts::eBoundLoc1]);
	    // const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(meas[Acts::eBoundPhi], meas[Acts::eBoundTheta]);
	    resi.push_back({local.x(),local.y(),parameter.position(gctx).x(), parameter.position(gctx).y(), parameter.position(gctx).z(), cluster,residual(Acts::eBoundLoc0),parameter});
	    ATH_MSG_DEBUG("  residual/global position:   " << residual(Acts::eBoundLoc0)<<" "<<parameter.position(gctx).x()<<" "<<parameter.position(gctx).y()<<" "<<parameter.position(gctx).z());
	    }
	    return true;
	});

      }

    } else {
      ATH_MSG_DEBUG("No fitted parameters for track");
    }
  }
  return resi;

}

//get the unbiased residual for IFT, i.e. remove whole IFT in the track fitting
//output is a vector of the outliers
//
std::vector<TSOS4Residual>
KalmanFitterTool::getUnbiasedResidual(const EventContext &ctx, const Acts::GeometryContext &gctx,
                      Trk::Track* inputTrack,  const Acts::BoundVector& /*inputVector*/,
                      bool /*isMC*/, double origin, std::vector<const Tracker::FaserSCT_Cluster*>& clusters, const Acts::BoundTrackParameters ini_Param) const {
  std::vector<TSOS4Residual> resi;
  resi.clear();
  std::vector<FaserActsRecMultiTrajectory> myTrajectories;

  if (!inputTrack->measurementsOnTrack() || inputTrack->measurementsOnTrack()->size() < m_minMeasurements) {
    ATH_MSG_DEBUG("Input track has no or too little measurements and cannot be fitted");
    return resi;
  }

  if (!inputTrack->trackParameters() || inputTrack->trackParameters()->empty()) {
    ATH_MSG_DEBUG("Input track has no track parameters and cannot be fitted");
    return resi;
  }


  auto pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  Acts::MagneticFieldContext mfContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext = Acts::CalibrationContext();

  auto [sourceLinks, measurements] = getMeasurementsFromTrack(inputTrack/*, wafer_id*/, clusters);
  ATH_MSG_DEBUG("trackParameters: " << ini_Param.parameters().transpose());
  ATH_MSG_DEBUG("position: " << ini_Param.position(gctx).transpose());
  ATH_MSG_DEBUG("momentum: " << ini_Param.momentum().transpose());
  ATH_MSG_DEBUG("charge: " << ini_Param.charge());

  FaserActsOutlierFinder faserActsOutlierFinder{0};
  faserActsOutlierFinder.cluster_z=-1000000.;
  faserActsOutlierFinder.StateChiSquaredPerNumberDoFCut=10000.;
  Acts::KalmanFitterOptions<MeasurementCalibrator, FaserActsOutlierFinder, Acts::VoidReverseFilteringLogic>
      kfOptions(gctx, mfContext, calibContext, MeasurementCalibrator(measurements),
                faserActsOutlierFinder, Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                Acts::PropagatorPlainOptions(), &(*pSurface));
  kfOptions.multipleScattering = false;
  kfOptions.energyLoss = false;

  ATH_MSG_DEBUG("Invoke fitter");
  auto result = (*m_fit)(sourceLinks, ini_Param, kfOptions);

  if (result.ok()) {
    const auto& fitOutput = result.value();
    if (fitOutput.fittedParameters) {
      const auto& params = fitOutput.fittedParameters.value();
      ATH_MSG_DEBUG("ReFitted parameters");
      ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
      ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
      ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
      ATH_MSG_DEBUG("  charge:   " << params.charge());
      using IndexedParams = std::unordered_map<size_t, Acts::BoundTrackParameters>;
      IndexedParams indexedParams;
      indexedParams.emplace(fitOutput.lastMeasurementIndex, std::move(params));
      std::vector<size_t> trackTips;
      trackTips.reserve(1);
      trackTips.emplace_back(fitOutput.lastMeasurementIndex);
      myTrajectories.emplace_back(std::move(fitOutput.fittedStates),
                                  std::move(trackTips),
                                  std::move(indexedParams));
      for (const FaserActsRecMultiTrajectory &traj : myTrajectories) {
	const auto& mj = traj.multiTrajectory();
	const auto& trackTips = traj.tips();
	if(trackTips.empty())continue;
	auto& trackTip = trackTips.front();
	mj.visitBackwards(trackTip, [&](const auto &state) {
	    auto typeFlags = state.typeFlags();
	    if (not typeFlags.test(Acts::TrackStateFlag::OutlierFlag))
	    {
	    return true;
	    }

	    if (state.hasUncalibrated()&&state.hasSmoothed()) {
	Acts::BoundTrackParameters parameter(
	    state.referenceSurface().getSharedPtr(),
	    state.smoothed(),
	    state.smoothedCovariance());
	    // auto covariance = state.smoothedCovariance();
	    auto H = state.effectiveProjector();
	    auto residual = state.effectiveCalibrated() - H * state.smoothed();
	    const Tracker::FaserSCT_Cluster* cluster = state.uncalibrated().hit();
	    // const auto& surface = state.referenceSurface();
	    Acts::BoundVector meas = state.projector().transpose() * state.calibrated();
	    Acts::Vector2 local(meas[Acts::eBoundLoc0], meas[Acts::eBoundLoc1]);
	    // const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(meas[Acts::eBoundPhi], meas[Acts::eBoundTheta]);
	    resi.push_back({local.x(),local.y(),parameter.position(gctx).x(), parameter.position(gctx).y(), parameter.position(gctx).z(), cluster,residual(Acts::eBoundLoc0),parameter});
	    }
	    return true;
	});

      }

    } else {
      ATH_MSG_DEBUG("No fitted parameters for track");
    }
  }
  return resi;

}

//get the unbiased residual for IFT, i.e. remove whole IFT in the track fitting
//output is a vector of the outliers
//
std::vector<TSOS4Residual>
KalmanFitterTool::getUnbiasedResidual(const EventContext &ctx, const Acts::GeometryContext &gctx,
                      Trk::Track* inputTrack,  const Acts::BoundVector& inputVector,
                      bool /*isMC*/, double origin) const {
  std::vector<TSOS4Residual> resi;
  resi.clear();
  std::vector<FaserActsRecMultiTrajectory> myTrajectories;

  if (!inputTrack->measurementsOnTrack() || inputTrack->measurementsOnTrack()->size() < m_minMeasurements) {
    ATH_MSG_DEBUG("Input track has no or too little measurements and cannot be fitted");
    return resi;
  }

  if (!inputTrack->trackParameters() || inputTrack->trackParameters()->empty()) {
    ATH_MSG_DEBUG("Input track has no track parameters and cannot be fitted");
    return resi;
  }


  auto pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  Acts::MagneticFieldContext mfContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext = Acts::CalibrationContext();

  auto [sourceLinks, measurements] = getMeasurementsFromTrack(inputTrack/*, wafer_id*/);
  auto trackParameters = getParametersFromTrack(inputTrack->trackParameters()->front(), inputVector, origin);
  ATH_MSG_DEBUG("trackParameters: " << trackParameters.parameters().transpose());
  ATH_MSG_DEBUG("position: " << trackParameters.position(gctx).transpose());
  ATH_MSG_DEBUG("momentum: " << trackParameters.momentum().transpose());
  ATH_MSG_DEBUG("charge: " << trackParameters.charge());


  FaserActsOutlierFinder faserActsOutlierFinder{0};
  faserActsOutlierFinder.cluster_z=-1000000.;
  faserActsOutlierFinder.StateChiSquaredPerNumberDoFCut=10000.;
  Acts::KalmanFitterOptions<MeasurementCalibrator, FaserActsOutlierFinder, Acts::VoidReverseFilteringLogic>
      kfOptions(gctx, mfContext, calibContext, MeasurementCalibrator(measurements),
                faserActsOutlierFinder, Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                Acts::PropagatorPlainOptions(), &(*pSurface));
  kfOptions.multipleScattering = false;
  kfOptions.energyLoss = false;

  ATH_MSG_DEBUG("Invoke fitter");
  auto result = (*m_fit)(sourceLinks, trackParameters, kfOptions);

  if (result.ok()) {
    const auto& fitOutput = result.value();
    if (fitOutput.fittedParameters) {
      const auto& params = fitOutput.fittedParameters.value();
      ATH_MSG_DEBUG("ReFitted parameters");
      ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
      ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
      ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
      ATH_MSG_DEBUG("  charge:   " << params.charge());
      using IndexedParams = std::unordered_map<size_t, Acts::BoundTrackParameters>;
      IndexedParams indexedParams;
      indexedParams.emplace(fitOutput.lastMeasurementIndex, std::move(params));
      std::vector<size_t> trackTips;
      trackTips.reserve(1);
      trackTips.emplace_back(fitOutput.lastMeasurementIndex);
      myTrajectories.emplace_back(std::move(fitOutput.fittedStates),
                                  std::move(trackTips),
                                  std::move(indexedParams));
      for (const FaserActsRecMultiTrajectory &traj : myTrajectories) {
	const auto& mj = traj.multiTrajectory();
	const auto& trackTips = traj.tips();
	if(trackTips.empty())continue;
	auto& trackTip = trackTips.front();
	mj.visitBackwards(trackTip, [&](const auto &state) {
	    auto typeFlags = state.typeFlags();
	    if (not typeFlags.test(Acts::TrackStateFlag::OutlierFlag))
	    {
	    return true;
	    }

	    if (state.hasUncalibrated()&&state.hasSmoothed()) {
	Acts::BoundTrackParameters parameter(
	    state.referenceSurface().getSharedPtr(),
	    state.smoothed(),
	    state.smoothedCovariance());
	    // auto covariance = state.smoothedCovariance();
	    auto H = state.effectiveProjector();
	    auto residual = state.effectiveCalibrated() - H * state.smoothed();
	    const Tracker::FaserSCT_Cluster* cluster = state.uncalibrated().hit();
	    // const auto& surface = state.referenceSurface();
	    Acts::BoundVector meas = state.projector().transpose() * state.calibrated();
	    Acts::Vector2 local(meas[Acts::eBoundLoc0], meas[Acts::eBoundLoc1]);
	    // const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(meas[Acts::eBoundPhi], meas[Acts::eBoundTheta]);
resi.push_back({local.x(),local.y(),parameter.position(gctx).x(), parameter.position(gctx).y(), parameter.position(gctx).z(), cluster,residual(Acts::eBoundLoc0),parameter});
	    }
	    return true;
	    });

      }

    } else {
      ATH_MSG_DEBUG("No fitted parameters for track");
    }
  }
  return resi;

}

std::unique_ptr<Trk::Track>
KalmanFitterTool::fit(const EventContext &ctx, const Acts::GeometryContext &gctx,
                      Trk::Track* inputTrack, const Acts::BoundVector& inputVector,
                      bool isMC) const {
  std::unique_ptr<Trk::Track> newTrack = nullptr;
  std::vector<FaserActsRecMultiTrajectory> myTrajectories;

  if (!inputTrack->measurementsOnTrack() || inputTrack->measurementsOnTrack()->size() < m_minMeasurements) {
    ATH_MSG_DEBUG("Input track has no or too little measurements and cannot be fitted");
    return nullptr;
  }

  if (!inputTrack->trackParameters() || inputTrack->trackParameters()->empty()) {
    ATH_MSG_DEBUG("Input track has no track parameters and cannot be fitted");
    return nullptr;
  }


  // set the start position 5 mm in front of the first track measurement
  double origin = inputTrack->trackParameters()->front()->position().z() - 10;

  auto pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  Acts::MagneticFieldContext mfContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext = Acts::CalibrationContext();

  auto [sourceLinks, measurements] = getMeasurementsFromTrack(inputTrack);
  auto trackParameters = getParametersFromTrack(inputTrack->trackParameters()->front(), inputVector, origin);
  ATH_MSG_DEBUG("trackParameters: " << trackParameters.parameters().transpose());
  ATH_MSG_DEBUG("position: " << trackParameters.position(gctx).transpose());
  ATH_MSG_DEBUG("momentum: " << trackParameters.momentum().transpose());
  ATH_MSG_DEBUG("charge: " << trackParameters.charge());

  FaserActsOutlierFinder faserActsOutlierFinder{0};
  faserActsOutlierFinder.cluster_z=-1000000.;
  faserActsOutlierFinder.StateChiSquaredPerNumberDoFCut=10000.;
  Acts::KalmanFitterOptions<MeasurementCalibrator, FaserActsOutlierFinder, Acts::VoidReverseFilteringLogic>
      kfOptions(gctx, mfContext, calibContext, MeasurementCalibrator(measurements),
                faserActsOutlierFinder, Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                Acts::PropagatorPlainOptions(), &(*pSurface));
  kfOptions.multipleScattering = false;
  kfOptions.energyLoss = false;

  ATH_MSG_DEBUG("Invoke fitter");
  auto result = (*m_fit)(sourceLinks, trackParameters, kfOptions);

  if (result.ok()) {
    const auto& fitOutput = result.value();
    if (fitOutput.fittedParameters) {
      const auto& params = fitOutput.fittedParameters.value();
      ATH_MSG_DEBUG("ReFitted parameters");
      ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
      ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
      ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
      ATH_MSG_DEBUG("  charge:   " << params.charge());
      using IndexedParams = std::unordered_map<size_t, Acts::BoundTrackParameters>;
      IndexedParams indexedParams;
      indexedParams.emplace(fitOutput.lastMeasurementIndex, std::move(params));
      std::vector<size_t> trackTips;
      trackTips.reserve(1);
      trackTips.emplace_back(fitOutput.lastMeasurementIndex);
      myTrajectories.emplace_back(std::move(fitOutput.fittedStates),
                                  std::move(trackTips),
                                  std::move(indexedParams));
    } else {
      ATH_MSG_DEBUG("No fitted parameters for track");
    }
    newTrack = m_createTrkTrackTool->createTrack(gctx, myTrajectories.back());
  }

  if (m_statesWriter && !m_noDiagnostics) {
    StatusCode statusStatesWriterTool = m_trajectoryStatesWriterTool->write(gctx, myTrajectories, isMC);
  }
  if (m_summaryWriter && !m_noDiagnostics) {
    StatusCode statusSummaryWriterTool = m_trajectorySummaryWriterTool->write(gctx, myTrajectories, isMC);
  }

  return newTrack;
}


namespace {

using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<>;
using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;
using Fitter = Acts::KalmanFitter<Propagator, Updater, Smoother>;

struct TrackFitterFunctionImpl
    : public KalmanFitterTool::TrackFitterFunction {
  Fitter trackFitter;

  TrackFitterFunctionImpl(Fitter &&f) : trackFitter(std::move(f)) {}

  KalmanFitterTool::TrackFitterResult operator()(
      const std::vector<IndexSourceLink> &sourceLinks,
      const KalmanFitterTool::TrackParameters &initialParameters,
      const KalmanFitterTool::TrackFitterOptions &options)
  const override {
    return trackFitter.fit(sourceLinks, initialParameters, options);
  };
};

}  // namespace


std::shared_ptr<KalmanFitterTool::TrackFitterFunction>
KalmanFitterTool::makeTrackFitterFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  auto stepper = Stepper(std::move(magneticField));
  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = false;
  cfg.resolveMaterial = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  Fitter trackFitter(std::move(propagator));
  return std::make_shared<TrackFitterFunctionImpl>(std::move(trackFitter));
}


Acts::MagneticFieldContext KalmanFitterTool::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};
  return Acts::MagneticFieldContext(fieldCondObj);
}


std::tuple<std::vector<IndexSourceLink>, std::vector<Measurement>>
KalmanFitterTool::getMeasurementsFromTrack(Trk::Track *track, std::vector<const Tracker::FaserSCT_Cluster*>& clusters) const {
  const int kSize = 1;
      ATH_MSG_DEBUG("clusters in refit "<<clusters.size());
  std::array<Acts::BoundIndices, kSize> Indices = {Acts::eBoundLoc0};
  using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, kSize>;
  using IdentifierMap = std::map<Identifier, Acts::GeometryIdentifier>;

  std::shared_ptr<IdentifierMap> identifierMap = m_trackingGeometryTool->getIdentifierMap();
  std::vector<IndexSourceLink> sourceLinks;
  std::vector<Measurement> measurements;
  for(auto cluster : clusters){
      Identifier id = cluster->identify();
      int ista = m_idHelper->station(id);
      int ilay = m_idHelper->layer(id);
      ATH_MSG_DEBUG("station/layer "<<ista<<" "<<ilay);
      Identifier waferId = m_idHelper->wafer_id(id);
      if (identifierMap->count(waferId) != 0) {
      ATH_MSG_DEBUG("found the identifier "<<ista<<" "<<ilay);
        Acts::GeometryIdentifier geoId = identifierMap->at(waferId);
        IndexSourceLink sourceLink(geoId, measurements.size(), cluster);
        Eigen::Matrix<double, 1, 1> pos {cluster->localPosition().x()};
        Eigen::Matrix<double, 1, 1> cov {0.08 * 0.08 / 12};
        ThisMeasurement actsMeas(sourceLink, Indices, pos, cov);
        sourceLinks.push_back(sourceLink);
        measurements.emplace_back(std::move(actsMeas));
      }
  }
  for (const Trk::MeasurementBase *meas : *track->measurementsOnTrack()) {
    const auto* clusterOnTrack = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*>(meas);
    const Tracker::FaserSCT_Cluster* cluster = clusterOnTrack->prepRawData();
    if (clusterOnTrack) {
      Identifier id = clusterOnTrack->identify();
      Identifier waferId = m_idHelper->wafer_id(id);
      if (identifierMap->count(waferId) != 0) {
        Acts::GeometryIdentifier geoId = identifierMap->at(waferId);
        IndexSourceLink sourceLink(geoId, measurements.size(), cluster);
        Eigen::Matrix<double, 1, 1> pos {meas->localParameters()[Trk::locX],};
        Eigen::Matrix<double, 1, 1> cov {0.08 * 0.08 / 12};
        ThisMeasurement actsMeas(sourceLink, Indices, pos, cov);
        sourceLinks.push_back(sourceLink);
        measurements.emplace_back(std::move(actsMeas));
      }
    }
  }
  return std::make_tuple(sourceLinks, measurements);
}

std::tuple<std::vector<IndexSourceLink>, std::vector<Measurement>>
KalmanFitterTool::getMeasurementsFromTrack(Trk::Track *track, Identifier& /*wafer_id*/) const {
  const int kSize = 1;
  std::array<Acts::BoundIndices, kSize> Indices = {Acts::eBoundLoc0};
  using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, kSize>;
  using IdentifierMap = std::map<Identifier, Acts::GeometryIdentifier>;

  std::shared_ptr<IdentifierMap> identifierMap = m_trackingGeometryTool->getIdentifierMap();
  std::vector<IndexSourceLink> sourceLinks;
  std::vector<Measurement> measurements;
  for (const Trk::MeasurementBase *meas : *track->measurementsOnTrack()) {
    const auto* clusterOnTrack = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*>(meas);
    const Tracker::FaserSCT_Cluster* cluster = clusterOnTrack->prepRawData();
    if (clusterOnTrack) {
      Identifier id = clusterOnTrack->identify();
      Identifier waferId = m_idHelper->wafer_id(id);

      if (identifierMap->count(waferId) != 0) {
        Acts::GeometryIdentifier geoId = identifierMap->at(waferId);
        IndexSourceLink sourceLink(geoId, measurements.size(), cluster);
        Eigen::Matrix<double, 1, 1> pos {meas->localParameters()[Trk::locX],};
        Eigen::Matrix<double, 1, 1> cov {0.08 * 0.08 / 12};
        ThisMeasurement actsMeas(sourceLink, Indices, pos, cov);
        sourceLinks.push_back(sourceLink);
        measurements.emplace_back(std::move(actsMeas));
      }
    }
  }
  return std::make_tuple(sourceLinks, measurements);
}
std::tuple<std::vector<IndexSourceLink>, std::vector<Measurement>>
KalmanFitterTool::getMeasurementsFromTrack(Trk::Track *track) const {
  const int kSize = 1;
  std::array<Acts::BoundIndices, kSize> Indices = {Acts::eBoundLoc0};
  using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, kSize>;
  using IdentifierMap = std::map<Identifier, Acts::GeometryIdentifier>;

  std::shared_ptr<IdentifierMap> identifierMap = m_trackingGeometryTool->getIdentifierMap();
  std::vector<IndexSourceLink> sourceLinks;
  std::vector<Measurement> measurements;
  for (const Trk::MeasurementBase *meas : *track->measurementsOnTrack()) {
    const auto* clusterOnTrack = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*>(meas);
    const Tracker::FaserSCT_Cluster* cluster = clusterOnTrack->prepRawData();
    if (clusterOnTrack) {
      Identifier id = clusterOnTrack->identify();
      Identifier waferId = m_idHelper->wafer_id(id);
      if (identifierMap->count(waferId) != 0) {
        Acts::GeometryIdentifier geoId = identifierMap->at(waferId);
        IndexSourceLink sourceLink(geoId, measurements.size(), cluster);
        Eigen::Matrix<double, 1, 1> pos {meas->localParameters()[Trk::locX],};
        Eigen::Matrix<double, 1, 1> cov {0.08 * 0.08 / 12};
        ThisMeasurement actsMeas(sourceLink, Indices, pos, cov);
        sourceLinks.push_back(sourceLink);
        measurements.emplace_back(std::move(actsMeas));
      }
    }
  }
  return std::make_tuple(sourceLinks, measurements);
}


Acts::BoundTrackParameters
KalmanFitterTool::getParametersFromTrack(const Trk::TrackParameters *inputParameters,
                                         const Acts::BoundVector& inputVector, double origin) const {
  using namespace Acts::UnitLiterals;
  std::shared_ptr<const Acts::Surface> pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  auto atlasParam = inputParameters->parameters();
  auto center = inputParameters->associatedSurface().center();
  // Acts::BoundVector params {center[Trk::locY], center[Trk::locX],
  //                           atlasParam[Trk::phi0], atlasParam[Trk::theta],
  //                           charge / (inputParameters->momentum().norm() * 1_MeV),
  //                           0.};
  Acts::BoundVector params;
  if (inputVector == Acts::BoundVector::Zero()) {
    params(0.0) = center[Trk::locY];
    params(1.0) = center[Trk::locX];
    params(2.0) = atlasParam[Trk::phi0];
    params(3.0) = atlasParam[Trk::theta];
    params(4.0) = inputParameters->charge() / (inputParameters->momentum().norm() * 1_MeV);
    params(5.0) = 0.;
  } else {
    params = inputVector;
  }
  Acts::BoundSymMatrix cov = Acts::BoundSymMatrix::Identity();
  cov.topLeftCorner(5, 5) = *inputParameters->covariance();

  for(int i=0; i < cov.rows(); i++){
    cov(i, 4) = cov(i, 4)/1_MeV;
  }
  for(int i=0; i < cov.cols(); i++){
    cov(4, i) = cov(4, i)/1_MeV;
  }
  for (int i = 0; i < 6; ++i) {
    cov(i,i) = m_seedCovarianceScale * cov(i,i);
  }

  return Acts::BoundTrackParameters(pSurface, params, inputParameters->charge(), cov);
}
